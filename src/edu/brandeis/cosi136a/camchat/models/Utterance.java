package edu.brandeis.cosi136a.camchat.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Utterance {

	public enum Speaker {
		PHONE, USER
	}
	
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss",Locale.US);
	
	private String mId;
	private Speaker mSpeaker;
	private String mSentence;
	private Date mCreated;

	public Utterance(Speaker speaker, String sentence) {
		mId = UUID.randomUUID().toString();
		mSpeaker = speaker;
		mSentence = sentence;
		mCreated = new Date();
	}
	
	public String getId(){
		return mId;
	}

	public String getSentence() {
		return mSentence;
	}

	public Speaker getSpeaker() {
		return mSpeaker;
	}

	public String toString() {
		return mSpeaker + "@" + DATE_FORMAT.format(mCreated) + " - " + mSentence;
	}
}
