package edu.brandeis.cosi136a.camchat.models;

import android.content.Intent;

public class CamChatIntent extends Intent {

	public static final int REQUEST_SPEECH_CODE = 100;
	public static final int REQUEST_PICTURE_CODE = 200;

	public static final String EXTRA_DELAY_PICTURE = "delay picture";
	public static final String EXTRA_NUM_PICTURES = "number of pictures";
	public static final String EXTRA_CAMERA_ID = "camera to use ";
	public static final String EXTRA_CAMERA_FLASH = "camera flash";
	
	public static final String EXTRA_RESULT_PICTURE_PATH = "picture path";

}
