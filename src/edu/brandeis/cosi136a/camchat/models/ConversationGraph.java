package edu.brandeis.cosi136a.camchat.models;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.res.XmlResourceParser;
import android.util.Log;

import edu.brandeis.cosi136a.camchat.MainActivity;
import edu.brandeis.cosi136a.camchat.algorithm.CamChatLM;
import edu.brandeis.cosi136a.camchat.models.Utterance.Speaker;

public class ConversationGraph {

	public class Node {

		private String mId;
		private String mQuestionRule;
		private Utterance mQuestion;
		private Utterance mResponse;
		// ruleref uri -> node id
		private Map<String, String> mPossibleResponses;

		public Node(String id) {
			mId = id;
			mQuestion = null;
			mResponse = null;
			mPossibleResponses = new HashMap<String, String>();
		}
		
		public Map<String,String> getPossibleResponses(){
			return mPossibleResponses;
		}

		public void addPossibleResponse(String response, String nodeId) {
			mPossibleResponses.put(response, nodeId);
		}

		public boolean isAction() {
			return mId.charAt(0) == 'A';
		}

		public boolean isQuestion() {
			return mId.charAt(0) == 'Q';
		}

		public void setQuestionRule(String r) {
			mQuestionRule = r;
		}

		public String getQuestionRule() {
			return mQuestionRule;
		}

		public void setQuestion(Utterance q) {
			mQuestion = q;
		}

		public Utterance getQuestion() {
			return mQuestion;
		}

		public void setResponse(Utterance r) {
			mResponse = r;
		}

		public Utterance getResponse() {
			return mResponse;
		}

		public String getId() {
			return mId;
		}
		
		public String toString(){
			return "{ id: " + mId + "}";
		}

	}

	private static final String TAG = "kh";

	private CamChatLM mDeviceLM;
	private CamChatLM mUserLM;
	private Map<String, Node> mNodes;
	private List<Node> mPath;
	private Stack<Utterance> mConversation;
	private String mStartId;
	private Node mCurrent;

	public ConversationGraph(XmlPullParser dialogue, XmlPullParser device,
			XmlPullParser user) {
		mDeviceLM = new CamChatLM(device);
		mUserLM = new CamChatLM(user);
		mNodes = new HashMap<String, Node>();
		mPath = new LinkedList<Node>();
		mConversation = new Stack<Utterance>();
		parseXML(dialogue);
		mCurrent = mNodes.get(mStartId);
	}

	public void parseXML(XmlPullParser xpp) {

		try {

			int eventType = xpp.getEventType();

			Node curr = null;
			while (eventType != XmlPullParser.END_DOCUMENT) {

				if (eventType == XmlPullParser.START_DOCUMENT) {

				} else if (eventType == XmlPullParser.START_TAG) {

					if (xpp.getName().equalsIgnoreCase("action")) {

						curr = new Node(xpp.getAttributeValue(null, "id"));
						curr.setQuestionRule(xpp.getAttributeValue(null,
								"description"));
						curr.addPossibleResponse("*",
								xpp.getAttributeValue(null, "next"));
					} else if (xpp.getName().equalsIgnoreCase("questions")) {

						mStartId = xpp.getAttributeValue(null, "start");

					} else if (xpp.getName().equalsIgnoreCase("question")) {

						curr = new Node(xpp.getAttributeValue(null, "id"));
						curr.setQuestionRule(xpp.getAttributeValue(null,
								"ruleref"));

					} else if (xpp.getName().equalsIgnoreCase("response")) {

						String uri = xpp.getAttributeValue(null, "ruleref");
						String nodeId = xpp.getAttributeValue(null, "next");
						if (curr != null && uri != null && nodeId != null) {
							curr.addPossibleResponse(uri, nodeId);
						}
					}

				} else if (eventType == XmlPullParser.END_TAG) {

					if (xpp.getName().equalsIgnoreCase("action")) {

						if (curr != null) {
							mNodes.put(curr.getId(), curr);
							curr = null;
						}
					} else if (xpp.getName().equalsIgnoreCase("question")) {

						if (curr != null) {
							mNodes.put(curr.getId(), curr);
							curr = null;
						}
					}

				}

				try {
					eventType = xpp.next();
				} catch (IOException e) {
					Log.d(TAG, e.toString());
				}
			}
		} catch (XmlPullParserException e) {
			Log.d(TAG, e.toString());
		}
	}

	public CamChatLM getDeviceLM(){
		return mDeviceLM;
	}
	
	public CamChatLM getUserLM(){
		return mUserLM;
	}
	
	public String getNextQuestion() {

		if (mCurrent.getQuestion() == null) {

			// need to generate question
			return mDeviceLM.generate(mCurrent.getQuestionRule());

		} else {

			return mCurrent.getQuestion().getSentence();
		}
	}

	public Utterance lastSaid() {
		return mConversation.isEmpty() ? null : mConversation.peek();
	}

	public Node getCurrentNode() {
		return mCurrent;
	}

	public void moveToNode(String nodeId) {

		mPath.add(mCurrent);
		mCurrent = mNodes.get(nodeId);
		
	}

	public void addUtterance(Utterance u) {
		
		mConversation.push(u);
		
		Log.d(MainActivity.LOG_TAG,"mCurrent= " + mCurrent.toString());

		if (u.getSpeaker() == Speaker.USER) {

			mCurrent.setResponse(u);

		} else {

			mCurrent.setQuestion(u);
		}
	}

	public void reset() {
		mPath.clear();
		mConversation.clear();
		for (String id : mNodes.keySet()) {
			Node n = mNodes.get(id);
			n.setQuestion(null);
			n.setResponse(null);
		}
		mCurrent = mNodes.get(mStartId);
	}

	public List<Node> getPath(){
		return mPath;
	}
	
	public List<Utterance> getConversation() {
		return mConversation;
	}
}
