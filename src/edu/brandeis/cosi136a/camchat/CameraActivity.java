package edu.brandeis.cosi136a.camchat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import edu.brandeis.cosi136a.camchat.algorithm.CamChatAlgorithms;
import edu.brandeis.cosi136a.camchat.models.CamChatIntent;
import edu.brandeis.cosi136a.camchat.view.CameraPreview;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.util.Log;
import android.view.Menu;
import android.view.Surface;
import android.widget.FrameLayout;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class CameraActivity extends Activity {

	public static final int MEDIA_TYPE_IMAGE = 1;

	private Camera mCamera;
	private TextToSpeech mTTS;
	private CameraPreview mPreview;
	private Handler mHandler;
	private int mCount;

	private Camera.ShutterCallback mShutter = new Camera.ShutterCallback() {

		@Override
		public void onShutter() {
			Log.d(MainActivity.LOG_TAG, "shutter");
		}
	};

	private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			Log.d(MainActivity.LOG_TAG, "onPictureTaken");

			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
			if (pictureFile == null) {
				Log.d(MainActivity.LOG_TAG,
						"Error creating media file, check storage permissions: ");
				return;
			}

			try {

				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();

				// notify gallery that picture exists
				Uri contentUri = Uri.fromFile(pictureFile);
				Intent mediaScanIntent = new Intent(
						Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, contentUri);
				CameraActivity.this.sendBroadcast(mediaScanIntent);

				// send path to picture back to main
				Intent returnIntent = new Intent();
				returnIntent.putExtra(CamChatIntent.EXTRA_RESULT_PICTURE_PATH,
						pictureFile.getAbsolutePath());
				CameraActivity.this.setResult(RESULT_OK, returnIntent);
				CameraActivity.this.finish();

			} catch (FileNotFoundException e) {
				Log.d(MainActivity.LOG_TAG, "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d(MainActivity.LOG_TAG,
						"Error accessing file: " + e.getMessage());
			}
		}
	};

	private Runnable mTakePicture = new Runnable() {

		@Override
		public void run() {

			updateUI();
			if (mCount == 0) {

				// Intent intent = CameraActivity.this.getIntent();
				// int numPictures;
				// try {
				// numPictures = intent.getIntExtra(
				// CamChatIntent.EXTRA_NUM_PICTURES, 1);
				// } catch (Exception e) {
				// Log.d(MainActivity.LOG_TAG, e.toString());
				// numPictures = 1;
				// }
				// Log.d(MainActivity.LOG_TAG, "num pictures: " + numPictures);
				// for (int i = 0; i < numPictures; i++) {

				mCamera.takePicture(mShutter, null, null, mPicture);

				// mCamera.startPreview();
				// }
			}
			mCount--;

		}

	};

	private void updateUI() {

		TextView countdownView = (TextView) findViewById(R.id.textview_countdown);
		countdownView.setText("" + mCount);
		if (mCount == 0) {
			speakOut("Say Cheese!");
		} else if (mCount <= 3) {
			speakOut("" + mCount);
		} else {
			mHandler.postDelayed(mTakePicture, 1000);
		}
	}

	private static String getAlbumName() {
		return "CamChat";
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type) {

		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		Log.d(MainActivity.LOG_TAG, Environment.getExternalStorageState());

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			File mediaStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					getAlbumName());

			// Create the storage directory if it does not exist
			if (!mediaStorageDir.exists()) {
				if (!mediaStorageDir.mkdirs()) {
					Log.d(MainActivity.LOG_TAG, "failed to create directory");
					return null;
				}
			}

			// Create a media file name
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());
			File mediaFile;
			if (type == MEDIA_TYPE_IMAGE) {
				mediaFile = new File(mediaStorageDir.getPath() + File.separator
						+ "IMG_" + timeStamp + ".jpg");
			} else {
				return null;
			}

			return mediaFile;
		}
		return null;

	}

	private OnUtteranceCompletedListener mTTSUtteranceCompletedlistener = new OnUtteranceCompletedListener() {

		@Override
		public void onUtteranceCompleted(String utteranceId) {
			Log.d(MainActivity.LOG_TAG,
					"Device finished speaking utterance id: " + utteranceId);
			if (mCount >= 0) {
				mHandler.postDelayed(mTakePicture, 1000);
			}
		}

	};

	private TextToSpeech.OnInitListener mTTSOnInitListener = new TextToSpeech.OnInitListener() {

		@Override
		public void onInit(int status) {
			if (status == TextToSpeech.SUCCESS) {

				Log.d(MainActivity.LOG_TAG, "Set up TTS");

				if (mTTS.setOnUtteranceCompletedListener(mTTSUtteranceCompletedlistener) == TextToSpeech.SUCCESS) {
					Log.d(MainActivity.LOG_TAG,
							"Registered UtteranceCompletedlistener");
				} else {
					Log.d(MainActivity.LOG_TAG,
							"Failed to register UtteranceCompletedlistener");
				}

				int result = mTTS.setLanguage(Locale.US);

				if (result == TextToSpeech.LANG_MISSING_DATA
						|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
					String msg = "This Language is not supported";
					Log.e(MainActivity.LOG_TAG, msg);
				} else {
					// allow user to speak again
				}

			} else {
				Log.e(MainActivity.LOG_TAG, "Initilization Failed!");
			}

		}
	};

	private void speakOut(String text) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, text);
		mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, params);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		Intent intent = this.getIntent();

		// seconds to wait before taking the photo
		mCount = intent.getIntExtra(CamChatIntent.EXTRA_DELAY_PICTURE, 3);
		if (mCount <= 0) {
			mCount = 3;
		}
		Log.d(MainActivity.LOG_TAG, "seconds before taking photo: " + mCount);

		// initialize text to speech
		mTTS = new TextToSpeech(this, mTTSOnInitListener);

		// Create an instance of Camera
		int cameraId = intent.getIntExtra(CamChatIntent.EXTRA_CAMERA_ID, 0);
		Log.d(MainActivity.LOG_TAG,"camera id: " + cameraId);
		mCamera = getCameraInstance(cameraId);
		
		// Set camera parameters
		Camera.Parameters params = mCamera.getParameters();
		
		// flash
		boolean flashOn = intent.getBooleanExtra(CamChatIntent.EXTRA_CAMERA_FLASH,false);
		params.setFlashMode(flashOn ? Camera.Parameters.FLASH_MODE_ON : Camera.Parameters.FLASH_MODE_OFF);
		
		mCamera.setParameters(params);

		// Create our Preview view and set it as the content of our
		// activity.

		mPreview = new CameraPreview(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);

		mHandler = new Handler();
		mHandler.postDelayed(mTakePicture, 1000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	protected void onDestroy() {

		if (mCamera != null) {
			mCamera.release();
			mCamera = null;
			Log.d(MainActivity.LOG_TAG, "Released camera");
		}

		if (mTTS != null) {
			mTTS.stop();
			mTTS.shutdown();
		}

		super.onDestroy();
	}

	private static void setCameraDisplayOrientation(Activity activity,
			int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}

	/** A safe way to get an instance of the Camera object. */
	private Camera getCameraInstance(int cameraId) {

		Camera c = null;
		try {
			c = Camera.open(cameraId);
		} catch (RuntimeException e) {
			// Camera is not available (in use or does not exist)
			Log.d(MainActivity.LOG_TAG,
					"Camera is being used by another process");
		}

		if (c != null) {
			setCameraDisplayOrientation(this, cameraId, c);
			Log.d(MainActivity.LOG_TAG, "Got camera");
		}
		return c; // returns null if camera is unavailable
	}

}
