package edu.brandeis.cosi136a.camchat;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;

import edu.brandeis.cosi136a.camchat.algorithm.*;
import edu.brandeis.cosi136a.camchat.models.CamChatIntent;
import edu.brandeis.cosi136a.camchat.models.Utterance;
import edu.brandeis.cosi136a.camchat.models.Utterance.Speaker;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.Camera;
import android.net.NetworkInfo;

import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MainActivity extends Activity implements
		OnSharedPreferenceChangeListener {

	public static final String LOG_TAG = "kh";
	private static final Class<?> ALGORITHMS_CLASS = KISSAlgorithms.class;

	private TextToSpeech mTTS;
	private CamChatAlgorithms mAlgorithms;
	private Handler mHandler;

	// **** UI Layout: Create/Destroy ****

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		setAlgorithms(prefs);
		// register listers for preference changes
		prefs.registerOnSharedPreferenceChangeListener(this);
		setupUI(prefs);

		// initialize Text To Speech engine
		mTTS = new TextToSpeech(this, mTTSOnInitListener);

		Context context = getApplicationContext();
		context.registerReceiver(mBroadcastReceiver, new IntentFilter(
				"android.net.conn.CONNECTIVITY_CHANGE"));

		checkDeviceSettings(context);

		mHandler = new Handler();

	}

	private void setAlgorithms(SharedPreferences prefs) {
		// initialize algorithms to use

		try {
			String algorithmKey = getString(R.string.pref_algorithm_key);
			String className = prefs.getString(algorithmKey,
					ALGORITHMS_CLASS.getName());
			Log.d(LOG_TAG,
					"algorithms class: " + className);
			Class<?> cl = Class.forName(ALGORITHMS_CLASS.getName());
			Constructor<?> cons = cl.getConstructor(XmlPullParser.class,
					XmlPullParser.class, XmlPullParser.class);
			Resources res = getResources();
			XmlPullParser dialogue = res.getXml(R.xml.dialogue);
			XmlPullParser device = res.getXml(R.xml.grammar_device);
			XmlPullParser user = res.getXml(R.xml.grammar_user);
			mAlgorithms = (CamChatAlgorithms) cons.newInstance(dialogue,
					device, user);
			Log.d(LOG_TAG, "initialized aglorithms");

		} catch (InstantiationException e) {
			Log.e(LOG_TAG, "InstantiationException: " + e.getMessage());
		} catch (IllegalAccessException e) {
			Log.e(LOG_TAG, "IllegalAccessException: " + e.getMessage());
		} catch (NoSuchMethodException e) {
			Log.e(LOG_TAG, "NoSuchMethodException: " + e.getMessage());
		} catch (IllegalArgumentException e) {
			Log.e(LOG_TAG, "IllegalArgumentException: " + e.getMessage());
		} catch (InvocationTargetException e) {
			Log.e(LOG_TAG, "InvocationTargetException: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			Log.e(LOG_TAG, "ClassNotFoundException: " + e.getMessage());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		case R.id.action_about:
			intent = new Intent(this, AboutActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onDestroy() {
		if (mTTS != null) {
			mTTS.stop();
			mTTS.shutdown();
		}
		super.onDestroy();
	}

	// **** Helper functions *****

	private void setupUI(SharedPreferences prefs) {

		TextView tv = (TextView) findViewById(R.id.display_settings);
		tv.setMovementMethod(new ScrollingMovementMethod());

		RelativeLayout view = (RelativeLayout) findViewById(R.id.utterance_container);
		if (prefs.getBoolean("debug_mode", false)) {
			view.setVisibility(View.VISIBLE);
		} else {
			view.setVisibility(View.GONE);
		}
	}

	private void checkDeviceSettings(Context context) {

		if (SpeechRecognizer.isRecognitionAvailable(context)) {
			writeToConsole("Speech recognition is available.");
		} else {
			writeToConsole("Speech recognition is NOT available.");
		}

		if (context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			writeToConsole("Device has " + Camera.getNumberOfCameras()
					+ " cameras.");
		}

	}

	private void writeToConsole(CharSequence message) {

		TextView settingsView = (TextView) findViewById(R.id.display_settings);
		String msg = message + "\r\n";
		settingsView.append(msg);
		Log.d(LOG_TAG, msg);
	}

	private Runnable mNextAction = new Runnable() {

		@Override
		public void run() {

			if (mAlgorithms == null) {
				Log.d(LOG_TAG, "algorithms not initialized");
				return;
			}

			Log.d(LOG_TAG, "nextAction");

			if (mAlgorithms.shouldExit()) {
				Log.d(LOG_TAG, "exit");

				Utterance u = mAlgorithms.getDeviceUtterance();
				speakOut(u.getSentence());
				mAlgorithms.reset();

			} else if (mAlgorithms.shouldWait()) {
				Log.d(LOG_TAG, "wait");
				
				long millis = mAlgorithms.getSecondsToWait() * 1000;
				Log.d(LOG_TAG, "millis: " + millis);
				if (millis > 0) {
					Log.d(LOG_TAG, "Wait for " + millis + " milliseconds");
					speakOut("I will wait for " + mAlgorithms.getSecondsToWait() + " seconds");
					mHandler.postDelayed(mNextAction, millis);
					mAlgorithms.reset();
				}

			} else if (mAlgorithms.shouldTakePicture()) {

				Log.d(LOG_TAG, "takePicture");

				Intent intent = new Intent(MainActivity.this,
						CameraActivity.class);
				intent.putExtra(CamChatIntent.EXTRA_DELAY_PICTURE,
						mAlgorithms.delayPictureBy());
				intent.putExtra(CamChatIntent.EXTRA_CAMERA_ID,
						mAlgorithms.getCameraId(MainActivity.this));
				intent.putExtra(CamChatIntent.EXTRA_CAMERA_FLASH,
						mAlgorithms.useFlash());

				if (mAlgorithms.isMultiShot()) {
					intent.putExtra(CamChatIntent.EXTRA_NUM_PICTURES,
							mAlgorithms.getNumPictures());
				}
				startActivityForResult(intent,
						CamChatIntent.REQUEST_PICTURE_CODE);

			} else {

				// figure out what other info you need to take a picture

				Utterance.Speaker turn = mAlgorithms.turnToSpeak();

				Log.d(LOG_TAG, "who's turn is it to speak? " + turn);

				if (turn == Utterance.Speaker.USER) {

					startSpeechActivity();

				} else {

					Utterance u = mAlgorithms.getDeviceUtterance();
					mAlgorithms.addUtterance(u);
					speakOut(u);

				}

			}
		}

	};

	// **** Button Actions ****

	public void printConversation(View view) {
		Log.d(LOG_TAG, "Conversation: ");
		for (Utterance u : mAlgorithms.getConversation()) {
			writeToConsole(u.toString());
		}
	}

	// *** Text to Speech ****

	private OnUtteranceCompletedListener mTTSUtteranceCompletedlistener = new OnUtteranceCompletedListener() {

		@Override
		public void onUtteranceCompleted(String utteranceId) {
			Log.d(LOG_TAG, "onUtteranceCompleted(" + utteranceId + ")");
			mHandler.postAtTime(mNextAction, 1000);
		}

	};

	private TextToSpeech.OnInitListener mTTSOnInitListener = new TextToSpeech.OnInitListener() {

		@Override
		public void onInit(int status) {
			if (status == TextToSpeech.SUCCESS) {

				if (mTTS.setOnUtteranceCompletedListener(mTTSUtteranceCompletedlistener) == TextToSpeech.SUCCESS) {
					writeToConsole("Registered UtteranceCompletedListener");
				} else {
					writeToConsole("Failed to register UtteranceCompletedListener");
				}

				int result = mTTS.setLanguage(Locale.US);

				if (result == TextToSpeech.LANG_MISSING_DATA
						|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
					String msg = "This Language is not supported";
					Log.e(LOG_TAG, msg);
				} else {
					// allow user to speak again
					ImageButton speakButton = (ImageButton) findViewById(R.id.speak_button);
					speakButton.setEnabled(true);
				}

			} else {
				Log.e(MainActivity.LOG_TAG, "Initilization Failed!");
			}

		}
	};

	public void speakOut(Utterance u) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, u.getId());
		mTTS.speak(u.getSentence(), TextToSpeech.QUEUE_FLUSH, params);
	}

	public void speakOut(String sentence) {
		mTTS.speak(sentence, TextToSpeech.QUEUE_FLUSH, null);
	}

	public void onSubmit(View view) {

		EditText userInput = (EditText) findViewById(R.id.user_utterance_textbox);
		String sentence = userInput.getText().toString();
		userInput.setText("");

		Spinner speakerSpinner = (Spinner) findViewById(R.id.speaker_spinner);
		String selectedSpeaker = (String) speakerSpinner.getSelectedItem();

		Utterance.Speaker speaker;
		if (selectedSpeaker.compareToIgnoreCase("Device") == 0) {
			speaker = Utterance.Speaker.PHONE;
		} else {
			speaker = Utterance.Speaker.USER;
		}

		Utterance u = new Utterance(speaker, sentence);
		mAlgorithms.addUtterance(u);
		mHandler.postAtTime(mNextAction, 1000);
	}

	// **** Speech Recognition *****

	private void startSpeechActivity() {

		Log.d(LOG_TAG, "startSpeechActivity");

		if (mAlgorithms.hasInternet()) {

			Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			// required
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
					RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

			// optional
			// intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
			// intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"");
			intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);

			startActivityForResult(intent, CamChatIntent.REQUEST_SPEECH_CODE);

		}
	}

	public void promptUserSpeech(View view) {
		mHandler.post(mNextAction);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		try {
			if (requestCode == CamChatIntent.REQUEST_SPEECH_CODE) {
				handleSpeechResult(resultCode, data);
			} else if (requestCode == CamChatIntent.REQUEST_PICTURE_CODE) {
				handlePictureTakenResult(resultCode, data);
			}
		} catch (ActivityNotFoundException e) {
			Log.d(LOG_TAG, e.getMessage());
		}
	}

	private void handleSpeechResult(int resultCode, Intent data) {

		Log.d(LOG_TAG, "handleSpeechResult");

		if (resultCode == RESULT_OK) {

			Log.d(LOG_TAG, "RESULT_OK");

			ArrayList<String> matches = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			mAlgorithms.addUtterance(mAlgorithms.getUserUtterance(matches));
			mHandler.postAtTime(mNextAction, 1000);

		} else if (resultCode == RecognizerIntent.RESULT_AUDIO_ERROR) {
			Utterance u = new Utterance(Speaker.PHONE,
					"There was an audio error.");
			mAlgorithms.addUtterance(u);
			speakOut(u);
		} else if (resultCode == RecognizerIntent.RESULT_CLIENT_ERROR) {
			Log.d(LOG_TAG, "Error: " + resultCode);
			Utterance u = new Utterance(Speaker.PHONE,
					"There was a client error.");
			mAlgorithms.addUtterance(u);
			speakOut(u);
		} else if (resultCode == RecognizerIntent.RESULT_NETWORK_ERROR) {
			Log.d(LOG_TAG, "Error: " + resultCode);
			Utterance u = new Utterance(Speaker.PHONE,
					"There was a network error.");
			mAlgorithms.addUtterance(u);
			speakOut(u);
		} else if (resultCode == RecognizerIntent.RESULT_NO_MATCH) {
			Log.d(LOG_TAG, "Error: " + resultCode);
			Utterance u = new Utterance(Speaker.PHONE,
					"Sorry I didn't quite get that.");
			mAlgorithms.addUtterance(u);
			speakOut(u);
		} else if (resultCode == RecognizerIntent.RESULT_SERVER_ERROR) {
			Log.d(LOG_TAG, "Error: " + resultCode);
			Utterance u = new Utterance(Speaker.PHONE,
					"There was a server error.");
			mAlgorithms.addUtterance(u);
			speakOut(u);
		}

	}

	private void handlePictureTakenResult(int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			String path = data
					.getStringExtra(CamChatIntent.EXTRA_RESULT_PICTURE_PATH);

			showToastMessage("Saved picture to " + path);

			speakOut("I've saved the picture.");

			mAlgorithms.reset();
		}
	}

	public void showToastMessage(String message) {
		Log.d(LOG_TAG, message);
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

		String algorithmKey = getString(R.string.pref_algorithm_key);
		if (key.equalsIgnoreCase(algorithmKey)) {
			setAlgorithms(prefs);
		}

		String debugKey = getString(R.string.pref_debug_mode_key);
		if (key.equalsIgnoreCase(debugKey)) {
			RelativeLayout view = (RelativeLayout) findViewById(R.id.utterance_container);
			if (prefs.getBoolean(debugKey, false)) {
				view.setVisibility(View.VISIBLE);
			} else {
				view.setVisibility(View.GONE);
			}
		}
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			ConnectivityManager connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			boolean isConnected = activeNetInfo != null
					&& activeNetInfo.isConnectedOrConnecting();
			activeNetInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			isConnected = isConnected
					|| (activeNetInfo != null && activeNetInfo
							.isConnectedOrConnecting());
			if (isConnected) {
				writeToConsole("Connected to internet.");
			} else {
				writeToConsole("Not connected to internet");
			}
			if (mAlgorithms != null) {
				mAlgorithms.setHasInternet(isConnected);
			} else {
				Log.d(LOG_TAG, "algorithms not initialized yet");
			}
		}

	};

}
