package edu.brandeis.cosi136a.camchat.algorithm;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import edu.brandeis.cosi136a.camchat.MainActivity;

import android.util.Log;

public class CamChatLM {

	public class Symbol {

		private String mId;
		private boolean mIsOneOf;
		private List<String> mRHS;
		private int mMinRepeat;
		private int mMaxRepeat;

		public int getMinRepeat() {
			return mMinRepeat;
		}

		public int getMaxRepeat() {
			return mMaxRepeat;
		}

		public boolean isOneOf() {
			return mIsOneOf;
		}

		public List<String> getRHS() {
			return mRHS;
		}

		public Symbol(String id, boolean isOneOf, int minRepeat, int maxRepeat) {
			mId = id;
			mIsOneOf = isOneOf;
			mMinRepeat = minRepeat;
			mMaxRepeat = maxRepeat;
			mRHS = new LinkedList<String>();
		}

		public String getId() {
			return mId;
		}

		public void addSymbolToRHS(String s) {
			mRHS.add(s);
		}

		public String toString() {
			String result = mId + "->";
			for (String s : mRHS) {
				result += "(" + s + ")";
				if (mIsOneOf) {
					result += " | ";
				} else {
					result += " ";
				}
			}
			return result.replaceAll("[(\\s)(\\|)]$", "");
		}
	}

	private static String TAG = "kh";
	private static long NEXT_ID = 0;
	private static Random mRandom = new Random();
	private Map<String, Symbol> mSymbols;
	private Stack<Symbol> mStack;
	private Stack<Boolean> mHasRepeatStack;
	private String mStartId;

	public CamChatLM(XmlPullParser xrp) {
		try {
			parseLM(xrp);
			for (String s : mSymbols.keySet()) {
				Log.d(TAG, mSymbols.get(s).toString());
			}
		} catch (Exception e) {
			String err = "";
			for (StackTraceElement s : e.getStackTrace()) {
				err += s.toString();
			}
			Log.d(TAG, "error: " + err);
		}
	}

	public static String nextId(String prefix) {
		return prefix + ":" + (++NEXT_ID);
	}

	public List<String> evaluate(Symbol s, List<String> output) {

		if (s == null) {
			Log.d(TAG, "evaluate: symbol is null");
			return output;
		}

		int min = s.getMinRepeat();
		if (min < 0) {

			min = 1;
		}
		int max = s.getMaxRepeat();
		if (max < 0) {

			max = 1;
		}

		if (min == 0) {

			if (mRandom.nextInt(1) == 0) {

				// occurs
				min = 1;
				max = mRandom.nextInt(s.getMaxRepeat()) + 1;

			} else {

				// never occurs
				min = max + 1;

			}
		}

		int i = min;
		while (i <= max) {

			if (s.isOneOf()) {

				// randomly select a word
				if (!s.getRHS().isEmpty()) {

					String word = s.getRHS().get(
							mRandom.nextInt(s.getRHS().size()));
					if (word.contains(":")) {

						output = evaluate(mSymbols.get(word), output);
					} else {

						output.add(word);
					}
				}

			} else {

				for (String id : s.getRHS()) {

					if (id.contains(":")) {

						// It's a symbol with a RHS!
						output = evaluate(mSymbols.get(id), output);

					} else {

						// Terminal!
						output.add(id);
					}
				}
			}
			i++;
		}

		return output;

	}

	public String generate(String start) {

		StringBuffer sb = new StringBuffer();
		Symbol startSymbol = start == null ? mSymbols.get(mStartId) : mSymbols
				.get(start);
		List<String> result = evaluate(startSymbol, new LinkedList<String>());
		for (int j = 0; j < result.size(); j++) {
			sb.append(result.get(j));
			if (j != result.size() - 1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}

	public Map<String, String> extractInfo(String sentence) {

		Map<String, String> info = new HashMap<String, String>();

		// break sentence up into tokens
		String[] arr = sentence.split(" ");

		return info;

	}

	private void parseLM(XmlPullParser xrp) throws XmlPullParserException,
			IOException {

		Log.d(TAG, "parseLM");

		int eventType = xrp.getEventType();
		Symbol curr;
		String text = "";
		while (eventType != XmlPullParser.END_DOCUMENT) {

			if (eventType == XmlPullParser.START_DOCUMENT) {

				// Log.d(TAG, "eventType: START_DOCUMENT");

				mSymbols = new HashMap<String, Symbol>();
				mStack = new Stack<Symbol>();
				mHasRepeatStack = new Stack<Boolean>();

			} else if (eventType == XmlPullParser.START_TAG) {

				// Log.d(TAG, "eventType: START_TAG " + xrp.getName());

				if (xrp.getName().equalsIgnoreCase("grammar")) {

					mStartId = "id:" + xrp.getAttributeValue(null, "root");

				} else if (xrp.getName().equalsIgnoreCase("rule")) {

					String id = xrp.getAttributeValue(null, "id");
					curr = new Symbol("id:" + id, false, 1, 1);
					mStack.push(curr);

				} else if (xrp.getName().equalsIgnoreCase("ruleref")
						&& !mStack.isEmpty()) {

					Symbol parent = mStack.peek();
					parent.addSymbolToRHS(xrp.getAttributeValue(null, "uri"));

				} else if (xrp.getName().equalsIgnoreCase("one-of")) {

					curr = new Symbol(nextId("one-of"), true, 1, 1);
					mStack.push(curr);

				} else if (xrp.getName().equalsIgnoreCase("item")) {

					String repeatVal = xrp.getAttributeValue(null, "repeat");

					if (repeatVal != null) {
						String[] arr = repeatVal.split("-");
						int min = 1, max = 1;
						if (arr.length == 2) {

							try {
								min = Integer.parseInt(arr[0]);
							} catch (NumberFormatException e) {
								min = -1;
							}
							try {
								max = Integer.parseInt(arr[1]);
							} catch (NumberFormatException e) {
								max = -1;
							}
						}
						curr = new Symbol(nextId("item"), false, min, max);
						mStack.push(curr);

						mHasRepeatStack.push(true);
					} else {
						mHasRepeatStack.push(false);
					}

				}
			} else if (eventType == XmlPullParser.END_TAG) {

				// Log.d(TAG, "eventType: END_TAG " + xrp.getName());

				if (xrp.getName().equalsIgnoreCase("item")) {

					Symbol parent = mStack.peek();
					boolean hasRepeat = mHasRepeatStack.pop();
					if (text.length() > 0) {
						parent.addSymbolToRHS(text);
					}
					if (hasRepeat) {
						parent = mStack.pop();
						if (!mStack.isEmpty()) {
							Symbol grandparent = mStack.peek();
							grandparent.addSymbolToRHS(parent.getId());
						}
						mSymbols.put(parent.getId(), parent);
					}

				} else if (xrp.getName().equalsIgnoreCase("one-of")) {

					Symbol parent = mStack.pop();
					if (!mStack.isEmpty()) {
						Symbol grandparent = mStack.peek();
						grandparent.addSymbolToRHS(parent.getId());
					}
					mSymbols.put(parent.getId(), parent);

				} else if (xrp.getName().equalsIgnoreCase("rule")
						&& !mStack.isEmpty()) {

					Symbol parent = mStack.pop();
					mSymbols.put(parent.getId(), parent);
				}

				// clear text content
				text = "";

			} else if (eventType == XmlPullParser.TEXT) {
				text = xrp.getText();
			}

			eventType = xrp.next();

		}
		Log.d(TAG, "Finished Parsing");
	}

	public long score(String symbolId, String text) {
		Log.d(MainActivity.LOG_TAG, "score: " + symbolId + " text: " + text);
		StringBuffer sb = new StringBuffer();
		return walk(mSymbols.get(symbolId), text, sb).length();
	}

	private StringBuffer walk(Symbol s, String text, StringBuffer score) {

		for (String id : s.getRHS()) {

			if (id.contains(":")) {

				// It's a symbol with a RHS!
				score = walk(mSymbols.get(id), text, score);

			} else {

				// Terminal!
				if(id.toLowerCase().indexOf(text.toLowerCase()) >= 0){
					score.append("11111");
				}
				
				for (String word : text.split(" ")) {
					if (id.toLowerCase().contains(word.toLowerCase())) {
						score.append("1");
					}
				}
			}
		}

		return score;
	}

}
