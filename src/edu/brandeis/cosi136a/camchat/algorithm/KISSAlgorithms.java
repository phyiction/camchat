package edu.brandeis.cosi136a.camchat.algorithm;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera.Parameters;
import android.preference.PreferenceManager;
import android.util.Log;
import edu.brandeis.cosi136a.camchat.MainActivity;
import edu.brandeis.cosi136a.camchat.models.ConversationGraph;
import edu.brandeis.cosi136a.camchat.models.Utterance;
import edu.brandeis.cosi136a.camchat.models.Utterance.Speaker;

public class KISSAlgorithms extends CamChatAlgorithms {

	private int mSecondsBeforeSnapshot;
	private int mNumPictures;
	private int mSecondsToWait;

	public KISSAlgorithms(XmlPullParser dialogue, XmlPullParser device,
			XmlPullParser user) {
		super(dialogue, device, user);
		reset();
	}

	// **** Dialogue *****

	@Override
	public Utterance.Speaker turnToSpeak() {

		ConversationGraph.Node c = mConversation.getCurrentNode();
		if (c.isAction()) {
			moveToNextNode();
			Log.d(MainActivity.LOG_TAG,
					"mCurrent=" + mConversation.getCurrentNode());
		}

		// who spoke last?
		Utterance u1 = mConversation.lastSaid();
		if (u1 == null || shouldExit()) {
			// conversation just began or about to end
			return Speaker.PHONE;
		}

		if (u1.getSpeaker() == Speaker.PHONE) {
			// phone
			return Speaker.USER;
		} else if (u1.getSpeaker() == Speaker.USER) {
			// user
			return Speaker.PHONE;
		}
		return Speaker.PHONE;

	}

	@Override
	public Utterance getUserUtterance(List<String> results) {
		// assume first match is best
		String sentence = results.get(0);
		return new Utterance(Utterance.Speaker.USER, sentence);
	}

	@Override
	public Utterance getDeviceUtterance() {

		String sentence = "";
		if (!mHasInternet) {
			sentence = "Please connect to the internet via your wi-fi or mobile cellular network";
		} else {
			sentence = mConversation.getNextQuestion();
		}

		return new Utterance(Utterance.Speaker.PHONE, sentence);
	}

	private int parseTime(String s) {

		if (s.contains("few seconds") || s.contains("moment")) {
			return 3;
		}

		if (s.contains("few minutes")) {
			return 3 * 60;
		}

		return -1;
	}

	@Override
	public void addUtterance(Utterance u) {
		super.addUtterance(u);

		// TODO: improvements
	}

	// **** Actions ****

	@Override
	public boolean shouldTakePicture() {
		ConversationGraph.Node n = mConversation.getCurrentNode();
		return n.getId().equalsIgnoreCase("A1");
	}

	public boolean shouldWait() {
		ConversationGraph.Node n = mConversation.getCurrentNode();
		return n.getId().equalsIgnoreCase("A7")
				|| n.getId().equalsIgnoreCase("A8");
	}

	public int getSecondsToWait() {
		ConversationGraph.Node n = mConversation.getCurrentNode();
		if(n != null){
			if (n.getId().equalsIgnoreCase("A7")) {
				Log.d(MainActivity.LOG_TAG, "set seconds to wait to 3");
				mSecondsToWait = 30;
			} else if (n.getId().equalsIgnoreCase("A8")) {
				String s = n.getResponse().getSentence();
				Pattern p = Pattern.compile("\\d+");
				Matcher m = p.matcher(s);
				if (m.find()) {
					MatchResult r = m.toMatchResult();
					try {
						mSecondsToWait = Integer.parseInt(s.substring(r.start(),r.end()));
					} catch (NumberFormatException e) {
						mSecondsToWait = 30;
					}
				}else{
					mSecondsToWait = 30;
				}
			}
		}
		return mSecondsToWait;
	}

	@Override
	public boolean shouldExit() {
		if (!mHasInternet) {
			return true;
		}
		ConversationGraph.Node n = mConversation.getCurrentNode();
		return n != null && n.isAction() && n.getId().equalsIgnoreCase("A3");
	}

	@Override
	public void reset() {
		Log.d(MainActivity.LOG_TAG, "reset algorithms");
		mConversation.reset();
		mNumPictures = -1;
		mSecondsBeforeSnapshot = -1;
		mSecondsToWait = -1;
	}

	// **** Camera Settings ****

	@Override
	public int getCameraId(Context context) {
		int defaultCameraId = super.getCameraId(context);
		int cameraId = -1;
		for (ConversationGraph.Node n : mConversation.getPath()) {
			if (n.getId().equalsIgnoreCase("A5")) {
				// front facing
				cameraId = 1;
			} else if (n.getId().equalsIgnoreCase("A6")) {
				cameraId = 0;
			}
		}
		return cameraId < 0 ? defaultCameraId : cameraId;
	}

	@Override
	public boolean useFlash() {
		boolean turnFlashOn = false;
		for (ConversationGraph.Node n : mConversation.getPath()) {
			if (n.getId().equalsIgnoreCase("A2")) {
				turnFlashOn = true;
			} else if (n.getId().equalsIgnoreCase("A4")) {
				turnFlashOn = false;
			}
		}
		return turnFlashOn;
	}

	@Override
	public int delayPictureBy() {
		if (mSecondsBeforeSnapshot < 0) {
			return 3;
		} else {
			return mSecondsBeforeSnapshot;
		}
	}

	@Override
	public boolean isMultiShot() {
		return false;
	}

	@Override
	public int getNumPictures() {
		if (mNumPictures < 0) {
			return 3;
		} else {
			return mNumPictures;
		}
	}

}
