package edu.brandeis.cosi136a.camchat.algorithm;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera.Parameters;
import android.preference.PreferenceManager;
import android.util.Log;

import edu.brandeis.cosi136a.camchat.MainActivity;
import edu.brandeis.cosi136a.camchat.models.ConversationGraph;
import edu.brandeis.cosi136a.camchat.models.Utterance;
import edu.brandeis.cosi136a.camchat.models.Utterance.Speaker;

public abstract class CamChatAlgorithms {

	protected ConversationGraph mConversation;
	protected boolean mHasInternet;

	public CamChatAlgorithms(XmlPullParser dialogue, XmlPullParser device,
			XmlPullParser user) {
		mConversation = new ConversationGraph(dialogue, device, user);
		mHasInternet = false;
	}

	public void setHasInternet(boolean i) {
		mHasInternet = i;
	}

	public boolean hasInternet() {
		return mHasInternet;
	}

	// **** Actions ****

	/** stop talking to phone */
	public abstract boolean shouldExit();

	public abstract boolean shouldTakePicture();

	public abstract boolean shouldWait();

	public abstract int getSecondsToWait();

	/** clear conversation */
	public abstract void reset();

	// **** Dialogue ****

	public List<Utterance> getConversation() {
		return mConversation.getConversation();
	}

	public String getNextNodeId(ConversationGraph.Node n) {
		// m is rule uri -> node id
		if (n.isAction()) {
			return n.getPossibleResponses().get("*");
		} else {
			Map<String, String> m = n.getPossibleResponses();
			TreeMap<Long, String> results = new TreeMap<Long, String>();
			for (String uri : m.keySet()) {
				long s = mConversation.getUserLM().score(uri,
						n.getResponse().getSentence());
				results.put(s, m.get(uri));
			}
			Log.d(MainActivity.LOG_TAG, "scores: " + results.toString());
			return results.get(results.lastKey());
		}
	}

	protected void moveToNextNode() {
		String nextNodeId = getNextNodeId(mConversation.getCurrentNode());
		mConversation.moveToNode(nextNodeId);
	}

	public void addUtterance(Utterance u) {
		mConversation.addUtterance(u);
		if (u.getSpeaker() == Speaker.USER) {
			moveToNextNode();
		}
	}

	public abstract Utterance.Speaker turnToSpeak();

	public abstract Utterance getUserUtterance(List<String> results);

	public abstract Utterance getDeviceUtterance();

	// **** Camera Settings ****

	public int getCameraId(Context context) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String text = prefs.getString("default_camera", "0");
		return Integer.parseInt(text);
	}

	public abstract boolean useFlash();

	public abstract int delayPictureBy();

	public abstract boolean isMultiShot();

	/** number of pictures to take in multi-shot */
	public abstract int getNumPictures();

}
